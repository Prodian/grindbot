﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrindBot.Constants
{
    class Strings
    {
        internal const string SkipGossip =
            "arg = { GetGossipOptions() }; count = 1; typ = 2; while true do if arg[typ] ~= nil then if arg[typ] == 'vendor' then SelectGossipOption(count); break; else count = count + 1; typ = typ + 2; end else break end end";

        internal const string PosInfos =
            "px,py=GetPlayerMapPosition('player') px=px*100 py=py*100 justPos = format('%i/%i', px,py) posInfos = GetZoneText() .. ' ' .. justPos";

        internal const string GT_PosInfos = "posInfos";

        // TODO Needs to be removed when frames API is fixed
        internal const string IsVendorOpen =
            "if MerchantFrame:IsVisible() then vendorSh1 = 'true' else vendorSh1 = 'false' end";

        internal const string GT_IsVendorOpen = "vendorSh1";

        //Items which should never be sold
        internal static string[] ProtectedItems = { "Hearthstone", "Skinning Knife", "Mining Pick", "Soul Shard"};

        //Mine Nodes
        internal static string[] MineNames = {"Copper Vein", "Tin Vein", "Silver Vein", "Gold Vein", "Iron Vein", "Mithril Vein", "Truesilver Vein", "Thorium Vein"};
        
        //Herb Nodes
        internal static string[] HerbNames =
        {
            "Silverleaf", "Peacebloom", "Mageroyal", "Earthroot", "Briarthorn", "Bruiseweed", "Wild Steelbloom", "Fadeleaf", "Khadgar's Whisker",
            "Firebloom", "Sungrass", "Gromsblood", "Dreamfoil", "Icecaps", "Lotus"
        };

    }
}
