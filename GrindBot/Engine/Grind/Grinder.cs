﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using GrindBot.Engine.ProfileCreation;
using GrindBot.Objects;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Grindbot.Objects;
using GrindBot.Constants;
using GrindBot.Engine.Grind.States;
using GrindBot.Gui;
using GrindBot.Settings;
using ZzukBot.ExtensionFramework;
using ZzukBot.Game.Statics;
using ZzukBot.Helpers;
using ZzukBot.Mem;

namespace GrindBot.Engine.Grind
{
    internal class Grinder
    {
        internal static Grinder Access;
        private MainThread.Updater GrindUpdater;

        internal Grinder()
        {
            Access = this;
            //ErrorEnumHook.OnNewError += ErrorEnum_OnNewError;
        }


        public void LogToFile(string parFile, string parContent)
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "..\\" + parFile;
            File.AppendAllText(path, Environment.TickCount + @": " + parContent /*+ "\n" + @"###" + "\n" + "\n"*/);
        }

        public void PrintToChat(string parMessage)
        {
            Lua.Instance.Execute("DEFAULT_CHAT_FRAME:AddMessage('Grind -> " + parMessage + "')");
        }

        internal _StuckHelper StuckHelper { get; set; }
        // holds details about the currently loaded profile
        internal GrindProfile Profile { get; private set; }
        // the fsm for our grindbot
        internal _Engine Engine { get; private set; }
        // ingame informations
        internal _Info Info { get; private set; }
        // the last state that got run
        internal string LastState { get; private set; }

        private Form _selectorForm;
        private bool _runBot = false;


        private void ErrorEnum_OnNewError(WoWEventHandler.OnUiMessageArgs e)
        {
            if (e.Message.StartsWith("Target not"))
            {
                if (Access.Info.Combat.IsMovingBack) return;
                if (!Access.Info.Target.InSightWithTarget) return;
                // LosTimer is used within info.target.combatdistance
                Access.Info.Target.InSightWithTarget = false;
                Access.Info.Target.ResetToNormalAt = Environment.TickCount + 3000;
            }
            else if (e.Message.StartsWith("You must be standing") ||
                     e.Message.StartsWith("You need to be standing up to loot"))
            {
                Lua.Instance.Execute("SitOrStand()");
            }
            else if (e.Message.StartsWith("You cannot attack that target") || e.Message.StartsWith("Invalid target"))
            {
                var target = ObjectManager.Instance.Target;
                if (target == null) return;
                if (target.Health != 0)
                {
                    PrintToChat("Blacklisting mob.");
                    Access.Info.Combat.AddToBlacklist(target.Guid);
                }
            }
            else if (e.Message.StartsWith("You are facing the") || e.Message.StartsWith("Target needs to be"))
            {
                var tar = ObjectManager.Instance.Target;
                if (tar == null) return;
                //Are we moving?
                if (!ObjectManager.Instance.Player.IsCtmIdle ||
                    (ObjectManager.Instance.Player.MovementState & 0x1) == 0x1) return;

                ObjectManager.Instance.Player.Face(tar.Position);
                /*
                Access.Info.Target.FixFacing = true;
                Wait.Remove("FixFacingTimer");*/
            }
            else if (e.Message.StartsWith("Target too close"))
            {
                //Old backup logic was here
            }
            else if (e.Message.StartsWith("You can't carry any")
                     || e.Message.StartsWith("Requires Skinning"))
            {
                Access.Info.Loot.BlacklistCurrentLoot = true;
            }else if (e.Message.Contains("Level requirement do to that string"))
            {
                //Blacklist skin / herb / mine node
            }
        }

        /// <summary>
        ///     Code of the grindbot to run in Endscene
        /// </summary>
        private void RareCheck()
        {
            if (GrindSettings.Values.StopOnRare || GrindSettings.Values.NotifyOnRare)
            {
                if (Wait.For("RareScan12", 10000))
                {
                    
                    if (GrindSettings.Values.NotifyOnRare)
                    {  
                        var tmp = ObjectManager.Instance.Npcs.FirstOrDefault(i => i.CreatureRank == 2/*Rare Elite*/ && i.Health != 0);
                        if (tmp != null)
                        {
                            if (ObjectManager.Instance.Player.Position.GetDistanceTo(tmp.Position) < 25)
                            {
                                if (!Info.RareSpotter.Notified(tmp.Guid))
                                {
                                    Main.MainForm.updateNotification("Found a rare: " + tmp.Name);
                                }
                            }
                        }
                    }
                    if (GrindSettings.Values.StopOnRare)
                    {
                        Stop();
                    }
                }
            }
        }

        private void Refreshments()
        {
            Info.Latency = ObjectManager.Instance.Player.GetLatency() * 2;
            if(Wait.For("SpellBookUpdate", 30000))
                Spell.UpdateSpellbook();
        }

        private void ResetRelogTimer()
        {
            Wait.Remove("Relog");
            Wait.Remove("Relog2");
        }


        private void RelogRoutine()
        {
            switch (Login.Instance.LoginState.ToString())
            {
                case "login":
                    if (Wait.For("Relog", 5 * 60 * 1000, false))
                    {
                        if (!Wait.For("RelogReset", 2000, false))
                        {
                            if (!Wait.For("RelogReset2", 1, false))
                            {
                                Login.Instance.ResetLogin();
                            }
                        }
                        else
                        {
                            Login.Instance.DefaultServerLogin();
                            Wait.Remove("RelogReset");
                            Wait.Remove("RelogReset2");
                            Wait.Remove("StartGhostWalk");
                            Access.Info.SpiritWalk.GeneratePath = true;
                            Wait.Remove("Relog");
                        }
                    }
                    break;

                case "charselect":
                    if (Wait.For("EnterWorldClicker", 2000))
                        Login.Instance.EnterWorld();
                    break;
            }
        }



        internal void Stop()
        {
            if (ObjectManager.Instance.IsIngame)
            {
                ObjectManager.Instance.Player.CtmStopMovement();
            }
            //Hacks.Instance.Collision1 = false;
            //Hacks.Instance.Collision2 = false;
            // we arent running anymore
            
            Access = null;
            GrindUpdater.Stop();
            _runBot = false;
        }

        /// <summary>
        ///     Prepare everything (setup fsm, parse profile etc.)
        ///     return true if ingame and profile is valid
        /// </summary>
        internal bool Prepare(string parProfilePath)
        {
            if (!ObjectManager.Instance.IsIngame) return false;
            Profile = new GrindProfile(parProfilePath);
            if (!Profile.ProfileValid) return false;

            GrindSettings.Values.Load();

            CustomClasses.Instance.Load();

            //Choose a custom class
            if (!ChooseCustomClassByWowClass(ObjectManager.Instance.Player.Class))
            {
                MessageBox.Show(@"Unable to find a CC which we can use.");
                return _runBot = false;
            }



            //Call load on our chosen CC
            if (!CustomClasses.Instance.Current.Load())
            {
                //If the CC cannot load correctly we return alert the user
                MessageBox.Show(CustomClasses.Instance.Current.Name + @" - could not be loaded.");
                return _runBot = false;
            }
            
            WoWEventHandler.Instance.OnErrorMessage += (sender, args) =>
            {
                //Lua.Instance.Execute("DEFAULT_CHAT_FRAME:AddMessage('Grind: " + args.Message + "')");
                ErrorEnum_OnNewError(args);
            };

            StuckHelper = new _StuckHelper();

            Info = new _Info();

            if (
                ObjectManager.Instance.Player.Position.GetDistanceTo(
                    Access.Profile.Hotspots[Info.Waypoints.CurrentHotspotIndex].Position) > 200)
            {
                if (MessageBox.Show("Your character is far from the profile hotspots. \nPath generation may take a while. \nAre you sure you want to start?", "Long Path Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1) != System.Windows.Forms.DialogResult.Yes)
                    return _runBot = false;
            }
            Main.MainForm.UpdateControl("Loading Tiles", Main.MainForm.lGrindState);
            Info.Waypoints.LoadWaypoints();

            if (Info.Waypoints.CurrentWaypoints == null)
            {
                MessageBox.Show(
                    "PPather has failed to generate a path. \n Please restart the bot \n OR \n Try starting the bot in a different spot");
                return _runBot = false;
            }




            var tmpStates = new List<State>
            {
                new StateIdle(),
                new StateLoadNextHotspot(),
                new StateLoadNextWaypoint(),
                new StateWalk(),
                new StateFindTarget(),
                new StateApproachTarget(),
                new StateFight(),
                new StateRest(),
                new StateBuff()
            };

            if (GrindSettings.Values.LootUnits)
            {
                tmpStates.Add(new StateLoot());
            }

            tmpStates.Add(new StateReleaseSpirit());
            tmpStates.Add(new StateGhostWalk());
            tmpStates.Add(new StateWalkToRepair());
            tmpStates.Add(new StateWalkBackToGrind());
            tmpStates.Add(new StateAfterFightToPath());
            tmpStates.Add(new StateWaitAfterFight());
            tmpStates.Add(new StateDoRandomShit());

            if (GrindSettings.Values.BreakFor != 0 && GrindSettings.Values.ForceBreakAfter != 0)
            {
                Info.BreakHelper.SetBreakAt(60000);
                tmpStates.Add(new StateStartBreak());
            }

            if (Profile.RepairNPC != null)
                tmpStates.Add(new StateRepair());
            tmpStates.Sort();

            Engine = new _Engine(tmpStates);

            return true;
        }

        /// <summary>
        ///     Start running the fsm
        /// </summary>
        /// 
        internal bool Run()
        {
            GrindSettings.Values.Load();
            Access.Info.SpiritWalk.GeneratePath = true;

            //I'm throwing the entire logic into the mainthread updater,
            //this is bad pratice.
            //I'm only doing this because I don't have time to 
            //refactor the entire thing.
            //(Grindbot v1 essentially ran every frame)
            GrindUpdater = new MainThread.Updater(RunGrinder, 50);
            GrindUpdater.Start();

            Shared.ResetJumper();
            Wait.RemoveAll();
            return true;
        }

        private void RunGrinder()
        {
            if (!ObjectManager.Instance.IsIngame) return;
            try
            {
                ObjectManager.Instance.Player.AntiAfk();

                if (ObjectManager.Instance.IsIngame)
                {
                    RareCheck();
                    Refreshments();

                    LastState = Engine.Pulse();
                    Main.MainForm.UpdateControl("State: " + LastState, Main.MainForm.lGrindState);
                    ResetRelogTimer();

                    /*
                    LastState = Engine.Pulse();
                    Main.MainForm.UpdateControl("State: " + LastState, Main.MainForm.lGrindState);                    
                    ResetRelogTimer();*/
                }
                else
                {
                    if (Info.BreakHelper.NeedToBreak)
                        return;
                    RelogRoutine();
                }

            }
            catch (Exception e)
            {
                LogToFile(e.Message, "Exceptions.txt");
            }

            return;
        }

        internal void SetWaypointModifier(float parModifier)
        {
            GrindSettings.Values.WaypointModifier = parModifier;
        }

        //Function to select custom class
        internal bool ChooseCustomClassByWowClass(ZzukBot.Constants.Enums.ClassId wowClass)
        {
            //Collect a list of possible CCs to use
            List<int> possibleClasses = new List<int>();
            for (var i = 0; i < CustomClasses.Instance.Enumerator.Count; i++)
            {
                //If we find one for our class
                if (CustomClasses.Instance.Enumerator.ElementAt(i).Class == wowClass)
                {
                    //Add it to the list of possibles
                    possibleClasses.Add(i);
                    //Set it as the current CC to use
                    CustomClasses.Instance.SetCurrent(i);
                }
            }
            //If we had no CCs to use return false
            if (possibleClasses.Count != 0)
            {
                //If we have more than 1, let the user decide which one to use
                if (possibleClasses.Count > 1)
                {
                    //Load the form
                    _selectorForm = new CustomClassSelector(possibleClasses);
                    //Class to use is set in the GUI (nothing to return)
                    _selectorForm.ShowDialog();
                    _selectorForm.Close();
                    _selectorForm.Dispose();
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
