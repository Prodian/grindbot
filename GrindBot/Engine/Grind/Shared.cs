﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZzukBot.Helpers;
using ZzukBot.Game.Statics;

namespace GrindBot.Engine.Grind
{
    internal class Shared
    {
        private static readonly Random ran = new Random();

        internal static bool StartedMovement = false;

        private static readonly Random random = new Random();

        internal static void RandomJump()
        {
            if (Wait.For("RandomJump1", 1000))
            {
                if (Wait.For("RandomJump2", ran.Next(30000, 50000)))
                {
                    Lua.Instance.Execute("Jump()");
                }
            }
        }

        internal static void ResetJumper()
        {
            Wait.Remove("RandomJump1");
        }

        internal static void RandomResetJumper()
        {
            if (random.Next(1, 3) == 2)
                ResetJumper();
        }
    }
}