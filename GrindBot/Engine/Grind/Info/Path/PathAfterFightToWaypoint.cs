﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrindBot.Settings;
using ZzukBot.Game.Statics;
using ZzukBot.Helpers.PPather;

namespace GrindBot.Engine.Grind.Info.Path
{
    internal class _PathAfterFightToWaypoint
    {
        private bool _AdjustWaypoints;
        private bool ShouldReturn;

        internal bool AfterFightMovement { get; private set; }

        // Do we need to get back to our current path?

        internal void SetAfterFightMovement()
        {
            AfterFightMovement = true;
            _AdjustWaypoints = true;
        }

        internal void DisableAfterFightMovement()
        {
            AfterFightMovement = false;
        }

        internal void AdjustPath()
        {
            if (_AdjustWaypoints)
            {
                Grinder.Access.Info.Waypoints.SetCurrentWaypointToClosest();
                _AdjustWaypoints = false;
            }
        }

        internal bool NeedToReturn()
        {

            var diff =
                Grinder.Access.Info.Waypoints.CurrentWaypoint.GetDistanceTo(ObjectManager.Instance.Player.Position);
            if (diff > GrindSettings.Values.MaxDiffToWp)
            {
                ShouldReturn = true;
                if (AfterFightMovement)
                {
                    Shared.RandomResetJumper();
                    Grinder.Access.Info.Waypoints.SetCurrentWaypointToClosest();
                    AfterFightMovement = false;
                }
            }
            else if (diff <= 1.2f)
            {
                ShouldReturn = false;
            }
            return ShouldReturn;
        }
    }
}
