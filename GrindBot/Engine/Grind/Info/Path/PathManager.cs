﻿using GrindBot.Engine.Grind.Info.Path.Base;

namespace GrindBot.Engine.Grind.Info.Path
{
    internal class _PathManager
    {
        internal BasePath Ghostwalk;
        internal BasePath GrindToVendor;
        internal BasePath VendorToGrind;
    }
}
