﻿using System;
using System.Collections.Generic;
using ZzukBot.Constants;

using ZzukBot.Game.Statics;
using GrindBot.Objects;
using GrindBot.Constants;
using ZzukBot.Helpers.PPather;


namespace GrindBot.Engine.Grind.Info.Path.Base
{
    internal class SubPath
    {
        internal Waypoint EndPoint;

        internal ZzukBot.Helpers.PPather.Path FromStartToEnd;
        private int FromStartToEndIndex;
        internal Waypoint StartPoint;

        internal SubPath(Waypoint parStartPoint, Waypoint parEndPoint)
        {
            StartPoint = parStartPoint;
            EndPoint = parEndPoint;
            if (EndPoint.Type == Constants.Enums.PositionType.Hotspot)
            {
                FromStartToEnd = Navigation.Instance.CalculatePath(StartPoint.Position,
                    EndPoint.Position);
                FromStartToEndIndex = 1;
            }
        }

        internal Location CurrentWaypoint
        {
            get
            {
                if (NeedToLoadNextWaypoint)
                {
                    LoadNextWaypoint();
                }
                if (EndPoint.Type == Constants.Enums.PositionType.Waypoint)
                    return EndPoint.Position;
                return FromStartToEnd.Get(FromStartToEndIndex);
            }
        }

        internal bool ArrivedAtEndPoint
        {
            get
            {
                if (EndPoint.Type == Constants.Enums.PositionType.Hotspot &&
                    NeedToLoadNextWaypoint &&
                    FromStartToEnd.Count() - 1 == FromStartToEndIndex &&
                    EndPoint.Position.GetDistanceTo(FromStartToEnd.GetLast()) > 1)
                {
                    return true;
                }

                var res = false;

                if (!Grinder.Access.Info.Waypoints.NeedToLoadNextWaypoint(
                    EndPoint.Position))
                {
                    return false;
                }
                if (EndPoint.Type == Constants.Enums.PositionType.Waypoint)
                {
                    res = true;
                }
                else if (EndPoint.Type == Constants.Enums.PositionType.Hotspot
                         && FromStartToEndIndex >= FromStartToEnd.Count() -1)
                {
                    res = true;
                }
                return res;
            }
        }

        internal bool NeedToLoadNextWaypoint
        {
            get
            {
                if (EndPoint.Type == Constants.Enums.PositionType.Waypoint)
                    return false;

                if (Grinder.Access.Info.Waypoints.NeedToLoadNextWaypoint(
                    FromStartToEnd.Get(FromStartToEndIndex)))
                {
                    return true;
                }
                return false;
            }
        }

        internal void LoadNextWaypoint()
        {
            if (!NeedToLoadNextWaypoint) return;
            if (FromStartToEndIndex <= FromStartToEnd.Count() - 2)
            {
                FromStartToEndIndex++;
            }
        }

        internal void RegenerateWaypoints()
        {
            if (EndPoint.Type == Constants.Enums.PositionType.Hotspot)
            {
                FromStartToEnd = Navigation.Instance.CalculatePath(ObjectManager.Instance.Player.Position,
                    EndPoint.Position);
                FromStartToEndIndex = 1;
            }
        }
    }
}
