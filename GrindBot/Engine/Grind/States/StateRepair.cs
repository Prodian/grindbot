﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Grindbot.Objects;
using GrindBot.Constants;
using GrindBot.Engine.Grind.Info;
using GrindBot.Engine.Grind.Info.Path.Base;
using GrindBot.Objects;
using GrindBot.Settings;
using ZzukBot.Game.Statics;
using ZzukBot.Helpers;
using ZzukBot.Objects;
using Enums = ZzukBot.Constants.Enums;

namespace GrindBot.Engine.Grind.States
{
    internal class StateRepair : State
    {
        private bool BackToPath;

        internal override int Priority => 40;

        internal override bool NeedToRun => Grinder.Access.Info.Vendor.NeedToVendor
                                            || NeedToRepair();

        internal override string Name => "Vendoring / Repairing";

        internal List<int[]> ItemsToSell = new List<int[]>();

        Random rand = new Random();

        internal override void Run()
        {

            // close enough to vendor?
            if (ObjectManager.Instance.Player.Position.GetDistanceTo(Grinder.Access.Profile.RepairNPC.Coordinates) <
                4.0f)
            {
                //Slow down there!
                if (!Wait.For("SellItemsTimer112", 200 + rand.Next(400, 700))) return;

                Grinder.Access.Info.PathAfterFightToWaypoint.DisableAfterFightMovement();
                ObjectManager.Instance.Player.CtmStopMovement();

                // open vendor interface and skip gossip
                var vendor = ObjectManager.Instance.Npcs
                    .FirstOrDefault(i => i.Name == Grinder.Access.Profile.RepairNPC.Name);

                if (vendor == null) return;
                if(!ZzukBot.Game.Frames.MerchantFrame.IsOpen)
                {
                    Spell.Instance.CancelShapeshift();
                    vendor.Interact(false);
                    //Re-Do with frame API
                    Lua.Instance.Execute(Strings.SkipGossip);

                    //Clear the old item list
                    ItemsToSell = new List<int[]>();
                    //Build the new List
                    GetVendorItems();
                }
                else
                {
                    //#FIXME
                    // sell our shit

                    if (ItemsToSell.Count == 0)
                        BackToPath = true;
                    
                    //Sell the item, remove it from the list
                    SellAnItem();

                    if (BackToPath)
                    {
                        ZzukBot.Game.Frames.MerchantFrame.Instance.RepairAll();
                        Grinder.Access.Info.Vendor.DoneVendoring();
                        Grinder.Access.Info.Vendor.GoBackToGrindAfterVendor = true;

                        Grinder.Access.Info.Waypoints.ResetGrindPath();
                        var tmpList = new List<Waypoint>();

                        if (Grinder.Access.Profile.VendorHotspots != null &&
                            Grinder.Access.Profile.VendorHotspots.Length != 0)
                        {
                            for (var i = Grinder.Access.Profile.VendorHotspots.Length - 1; i >= 0; i--)
                            {
                                tmpList.Add(Grinder.Access.Profile.VendorHotspots[i]);
                            }
                        }
                        tmpList.Add(Grinder.Access.Profile.Hotspots[0]);

                        Grinder.Access.Info.PathManager.VendorToGrind = new BasePath(tmpList);
                    }
                }
            }
            // not close enough? lets travel to the vendor using another state!
            else
            {
                Grinder.Access.Info.Vendor.TravelingToVendor = true;
                var tmpList = new List<Waypoint>();

                //if (Grinder.Profile.VendorHotspots == null || Grinder.Profile.VendorHotspots.Length == 0)
                //    return;

                Grinder.Access.Info.Waypoints.RevertHotspotsToOriginal();
                var curHotspot = Grinder.Access.Info.Waypoints.CurrentHotspotIndex;

                if (curHotspot > 0)
                {
                    for (var i = curHotspot - 1; i >= 0; i--)
                    {
                        tmpList.Add(Grinder.Access.Profile.Hotspots[i]);
                    }
                }
                if (Grinder.Access.Profile.VendorHotspots != null && Grinder.Access.Profile.VendorHotspots.Length != 0)
                {
                    tmpList.AddRange(Grinder.Access.Profile.VendorHotspots);
                }
                var tmpWp = new Waypoint
                {
                    Position = Grinder.Access.Profile.RepairNPC.Coordinates,
                    Type = Constants.Enums.PositionType.Hotspot
                };
                tmpList.Add(tmpWp);

                Grinder.Access.Info.PathManager.GrindToVendor = new BasePath(tmpList);
            }
        }

        internal bool NeedToRepair()
        {
            //Check every 30 secs
            if (!Wait.For("DurabilityCheck", 30000)) return false;
            Console.WriteLine("Checking Durability.");
            float durability = 0.1f;
            float durabilityAll = 0.1f;
            foreach (var val in Enum.GetValues(typeof(Enums.EquipSlot)))
            {
                var item = Inventory.Instance.GetEquippedItem((Enums.EquipSlot)val);
                if (item == null) continue;
                durability = item.Durability + durability;
                durabilityAll = item.MaxDurability + durabilityAll;

            }
            float durabilityPercent = (durability/durabilityAll);
            float durabilityFuckThis = durabilityPercent*100;
            return durabilityFuckThis < GrindSettings.Values.RepairDurabilityPercentage;
        }

        //Try to sell an item - remove it from the list
        internal void SellAnItem()
        {
            if (ItemsToSell.Count != 0)
            {
                //Try to sell
                Lua.Instance.Execute("UseContainerItem(" + ItemsToSell[0][0] + "," + (ItemsToSell[0][1] +1) /*Lua doesn't quite line up*/ + ")");
                //Grinder.Access.PrintToChat(ItemsToSell[0][0]+","+(ItemsToSell[0][1] + 1));
                Wait.For("SellDelay", 500);
                //Remove it from the list
                ItemsToSell.RemoveAt(0);
            }
        }

        //Retrieve items to be vendored
        internal void GetVendorItems()
        {

            WoWItem ItemToVendor;


            //Check Bags
            for (int i = 0; i < 6; i++)
            {
                for (int s = 0; s < 22 /*Largest Possible Bag?*/; s++)
                {
                    ItemToVendor = Inventory.Instance.GetItem(i, s);
                    //Does an item exist at this slot?
                    if (ItemToVendor != null)
                    {
                        //Do we want to sell it?
                        if (!GrindSettings.Values.ProtectedItems.Contains(ItemToVendor.Name)
                            && ItemToVendor.Quality < GrindSettings.Values.KeepItemsFromQuality 
                            && !Constants.Strings.ProtectedItems.Contains(ItemToVendor.Name))
                        {
                            //Lua.Instance.Execute("DEFAULT_CHAT_FRAME:AddMessage('Adding item: "+ItemToVendor.Name+"')");
                            ItemsToSell.Add(new int[] { i, s });
                        }
                    }
                }
            }
        }
    }
}