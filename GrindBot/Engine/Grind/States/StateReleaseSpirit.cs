﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZzukBot.Game.Statics;
using Grindbot.Objects;
using ZzukBot.Helpers;

namespace GrindBot.Engine.Grind.States
{
    internal class StateReleaseSpirit : State
    {
        internal override int Priority => 55;

        internal override bool NeedToRun => ObjectManager.Instance.Player.IsDead;

        internal override string Name => "Releasing Spirit";

        internal override void Run()
        {
            if (!Wait.For("ReleasingSpirit", 250)) return;
            ObjectManager.Instance.Player.CtmStopMovement();
            Wait.Remove("StartGhostWalk");
            Lua.Instance.Execute("RepopMe()");
            Grinder.Access.Info.SpiritWalk.GeneratePath = true;
        }
    }
}
