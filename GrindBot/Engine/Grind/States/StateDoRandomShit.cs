﻿using System;
using System.Linq;
using Grindbot.Objects;
using GrindBot.Settings;
using ZzukBot.Constants;
using ZzukBot.Helpers;
using ZzukBot.Game.Statics;

namespace GrindBot.Engine.Grind.States
{
    internal class StateDoRandomShit : State
    {
        private bool EndRandomMove;
        private int EndRandomMoveAt;
        private Enums.ControlBits lastFlags;

        private readonly Random ran = new Random();

        internal override int Priority => 19;

        internal override bool NeedToRun => EndRandomMove || Wait.For("DoRandomShit", 5000);

        internal override string Name => "Do random shit";

        private Enums.ControlBits RandomFlag
        {
            get
            {
                var count = ran.Next(1, 3);
                var values = Enum.GetValues(typeof(Enums.ControlBits));
                var rFlag = Enums.ControlBits.Nothing;
                for (var i = 0; i < count; i++)
                {
                    rFlag |= (Enums.ControlBits)values.GetValue(ran.Next(values.Length));
                }
                return rFlag;
            }
        }

        internal override void Run()
        {
            // Removed for now.
            /*
            if (EndRandomMove)
            {
                if (Environment.TickCount > EndRandomMoveAt)
                {
                    //ObjectManager.Instance.Player.StopMovement(lastFlags);
                    EndRandomMove = false;
                }
                return;
            }
            */

            if (Wait.For("DRS_TarPlayer", ran.Next(4000, 8001)))
            {
                var players =
                    ObjectManager.Instance.Players.Where(
                        i =>
                            i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position) <=
                            GrindSettings.Values.MaxRandomTargetDistance)
                        .ToList();
                if (players.Count == 1) return;
                var ranValue = ran.Next(0, players.Count);
                var randomPlayer = players[ranValue];
                ObjectManager.Instance.Player.SetTarget(randomPlayer.Guid);
                Wait.For("TargetRest", ran.Next(100, 2000));
            }

            /*
            if (Wait.For("DRS_RandomMovement", ran.Next(8000, 16001)))
            {
                lastFlags = RandomFlag;

                //ObjectManager.Instance.Player.StartMovement(lastFlags);
                EndRandomMoveAt = Environment.TickCount + ran.Next(100, 150);
                EndRandomMove = true;
            }*/
        }
    }
}