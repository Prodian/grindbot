﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grindbot.Objects;
using ZzukBot.Constants;
using ZzukBot.ExtensionFramework;
using ZzukBot.Helpers;
using ZzukBot.Objects;
using ZzukBot.Game.Statics;
using ZzukBot.Helpers.PPather;

namespace GrindBot.Engine.Grind.States
{
    internal class StateFight : State
    {
        private bool CanceledLogout = true;

        private WoWUnit target;

        internal override int Priority => 50;

        internal override bool NeedToRun
        {
            get
            {
                if(ObjectManager.Instance.Player.IsInCombat)
                    return UnitInfo.Instance.NpcAttackers.Count != 0;
                return false;
            }
        } 

        internal override string Name => "Fight";

        internal override void Run()
        {

            if (Grinder.Access.Info.Vendor.GoBackToGrindAfterVendor
                || Grinder.Access.Info.Vendor.TravelingToVendor)
            {
                Grinder.Access.Info.Vendor.RegenerateSubPath = true;
            }

            Grinder.Access.Info.PathAfterFightToWaypoint.SetAfterFightMovement();
            Grinder.Access.Info.Combat.LastFightTick = Environment.TickCount;
            Grinder.Access.Info.Loot.RemoveRespawnedMobsFromBlacklist(UnitInfo.Instance.NpcAttackers);
            //Grinder.Access.Info.Target.SearchDirect = true;

            if (Grinder.Access.Info.BreakHelper.NeedToBreak)
            {
                if (CanceledLogout)
                {
                    Lua.Instance.Execute("CancelLogout()");
                    CanceledLogout = false;
                }
            }
            else
            {
                CanceledLogout = true;
            }

            target = ObjectManager.Instance.Target;
            if (target != null && target.IsInCombat)
            {


                var player = ObjectManager.Instance.Player;
                var IsCasting = !(player.Casting == 0 && player.Channeling == 0);
                var distanceToTarget =
                    player.Position.GetDistanceTo(target.Position);



                if (distanceToTarget >= Grinder.Access.Info.Target.CombatDistance && ((!IsCasting
                                                                                       &&
                                                                                       !Grinder.Access.Info.Combat
                                                                                           .IsMoving) ||
                                                                                      !Grinder.Access.Info.Target
                                                                                          .InSightWithTarget))
                {
                    var tu = Grinder.Access.Info.PathToUnit.ToUnit(target);
                    // If Generation fails just walk to them
                    if (tu == null)
                        player.CtmTo(target.Position);
                    if (tu.Item1)
                        player.CtmTo(tu.Item2);
                }
                else
                {
                    FixFacing(distanceToTarget, target.Position);
                        
                    CustomClasses.Instance.Current.OnFight();
                }
            }
            else
            {
                var tmp = UnitInfo.Instance.NpcAttackers.OrderBy(i => i.Health).FirstOrDefault();
                if (tmp == null) return;
                ObjectManager.Instance.Player.SetTarget(tmp);
            }
        }


        private void FixFacing(float distanceToTarget, Location faceLocation)
        {
            if (ObjectManager.Instance.Player.IsFacing(target.Position))
            {
                if (!CustomClasses.Instance.Current.SuppressBotMovement)
                {
                    if (distanceToTarget < 1f)
                    {
                        if (ObjectManager.Instance.Player.MovementState != 0x2)
                            Lua.Instance.Execute("MoveBackwardStart();");
                    }
                    else
                    {
                        Lua.Instance.Execute("MoveBackwardStop();");
                    }
                }
            }
            else if (!CustomClasses.Instance.Current.SuppressBotMovement)
            {
                if (ObjectManager.Instance.Player.FacingRelativeTo(faceLocation) > 0.7f ||
                    Wait.For("FixFacing", 2000))
                {
                    ObjectManager.Instance.Player.Face(faceLocation);
                }

            }
        }
    }
}
