﻿using Grindbot.Objects;
using ZzukBot.Game.Statics;

namespace GrindBot.Engine.Grind.States
{
    internal class StateAfterFightToPath : State
    {
        internal override int Priority => 33;

        internal override bool NeedToRun => Grinder.Access.Info.PathAfterFightToWaypoint.NeedToReturn();

        internal override string Name => "Back to path";

        internal override void Run()
        {
            Shared.RandomJump();

            ObjectManager.Instance.Player.CtmTo(Grinder.Access.Info.PathToPosition.ToPos(
                Grinder.Access.Info.Waypoints.CurrentWaypoint));
            // Nothing to do here
        }
    }
}
