﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grindbot.Objects;
using ZzukBot.Game.Statics;

namespace GrindBot.Engine.Grind.States
{
    internal class StateWalkToRepair : State
    {
        internal override int Priority => 41;

        internal override bool NeedToRun => Grinder.Access.Info.Vendor.TravelingToVendor;

        internal override string Name => "Travel to Vendor";

        internal override void Run()
        {
            if (Grinder.Access.Info.Vendor.RegenerateSubPath)
            {
                Grinder.Access.Info.PathManager.GrindToVendor.RegenerateSubPath();
                Grinder.Access.Info.Vendor.RegenerateSubPath = false;
            }

            var to = Grinder.Access.Info.PathManager.GrindToVendor.NextWaypoint;
            ObjectManager.Instance.Player.CtmTo(to);

            if (Grinder.Access.Info.PathManager.GrindToVendor.ArrivedAtDestination)
            {
                Grinder.Access.Info.Vendor.TravelingToVendor = false;
            }
        }
    }
}
