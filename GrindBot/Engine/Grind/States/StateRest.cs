﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grindbot.Objects;
using ZzukBot.ExtensionFramework;
using ZzukBot.Game.Statics;

namespace GrindBot.Engine.Grind.States
{
    internal class StateRest : State
    {
        internal override int Priority => 45;

        internal override bool NeedToRun => (Grinder.Access.Info.Rest.NeedToDrink || Grinder.Access.Info.Rest.NeedToEat) &&
                                            !ObjectManager.Instance.Player.IsInCampfire && !ObjectManager.Instance.Player.IsSwimming;

        internal override string Name => "Resting";

        private void ClearTarget()
        {
            var guid = ObjectManager.Instance.Player.Guid;
            var tarGuid = ObjectManager.Instance.Player.TargetGuid;
            if (tarGuid != 0
                &&
                tarGuid != guid && ObjectManager.Instance.Player.HasPet && tarGuid != ObjectManager.Instance.Pet.Guid)
            {
                ObjectManager.Instance.Player.SetTarget(guid);
            }
        }

        internal override void Run()
        {
            ObjectManager.Instance.Player.CtmStopMovement();
            ClearTarget();
            CustomClasses.Instance.Current.OnRest();
            Shared.ResetJumper();
        }
    }
}