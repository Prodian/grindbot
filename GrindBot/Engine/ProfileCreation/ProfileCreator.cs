﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using GrindBot.Constants;
using GrindBot.Gui;
using GrindBot.Settings;
using ZzukBot.Helpers.PPather;
using ZzukBot.Game.Statics;
using ZzukBot.Mem;

namespace GrindBot.Engine.ProfileCreation
{
    internal class ProfileCreator
    {
        private volatile bool boolAddFaction;
        private volatile bool boolAddGhostWaypoint;
        private volatile bool boolAddRepair;
        private volatile bool boolAddRestock;
        private volatile bool boolAddVendor;
        private volatile bool boolAddVendorWaypoint;
        private volatile bool boolAddId;
        // bools to check if user wants to add something to the profile
        private volatile bool boolAddWaypoint;

        // Are we running the profile creation?
        internal volatile bool IsCreatingProfile = false;
        // list of factions
        private volatile List<int> listFactions = new List<int>();
        // list of mob ids
        private volatile List<int> listIds = new List<int>();

        private volatile List<Tuple<Location, string, Enums.PositionType>> listGhostHotspots =
            new List<Tuple<Location, string, Enums.PositionType>>();

        private volatile List<Tuple<Location, string, Enums.PositionType>> listHotspots =
            new List<Tuple<Location, string, Enums.PositionType>>();

        // List of items to restock
        private volatile List<Structs.RestockItem> listRestockItems = new List<Structs.RestockItem>();

        private volatile List<Tuple<Location, string, Enums.PositionType>> listVendorHotspots =
            new List<Tuple<Location, string, Enums.PositionType>>();

        // Profile start
        private string profileStart = "";
        // Repair infos
        private Structs.NPC RepairNpc;
        // Restock infos
        private Structs.NPC RestockNpc;
        // Vendor infos (coords, name)
        private Structs.NPC VendorNpc;

        private MainThread.Updater ProfileUpdater;

        internal ProfileCreator()
        {
            Main.MainForm.lRecording.Text = "Recording";
            Main.MainForm.lRecording.Visible = true;
            ProfileUpdater = new MainThread.Updater(CreateProfile, 10);
            ProfileUpdater.Start();

        }

        internal bool Dispose()
        {
            var finalRes = true;
            var res = MessageBox.Show("Create the profile?", "", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes)
            {
                StopIt();
            }
            else if (listHotspots.Count < 2)
            {
                finalRes = false;
            }
            else
            {
                WriteToXmlProfile();
                StopIt();
            }
            return finalRes;
        }

        private void StopIt()
        {

            IsCreatingProfile = false;
            listFactions = new List<int>();
            listIds = new List<int>();
            listGhostHotspots = new List<Tuple<Location, string, Enums.PositionType>>();
            listHotspots = new List<Tuple<Location, string, Enums.PositionType>>();
            listRestockItems = new List<Structs.RestockItem>();
            listVendorHotspots = new List<Tuple<Location, string, Enums.PositionType>>();

            Main.MainForm.tbHotspots.Text = "";
            Main.MainForm.tbVendorHotspots.Text = "";
            Main.MainForm.tbGhostHotspots.Text = "";
            Main.MainForm.tbFactions.Text = "";
            Main.MainForm.tbRepair.Text = "";
            Main.MainForm.tbVendor.Text = "";
            Main.MainForm.tbRestock.Text = "";
            Main.MainForm.tbRestockItems.Text = "";
            Main.MainForm.tbIds.Text = "";

            Main.MainForm.lbIdCount.Text = "Count: ";
            Main.MainForm.lHotspotCount.Text = "Count: ";
            Main.MainForm.lVendorHotspotCount.Text = "Count: ";
            Main.MainForm.lFactionCount.Text = "Count: ";
            Main.MainForm.lGhostHotspotCount.Text = "Count: ";
            Main.MainForm.lRecording.Visible = false;

            ProfileUpdater?.Stop();
        }

        internal void ClearVendor()
        {
            Main.MainForm.tbVendor.Text = "";
            VendorNpc = null;
        }

        internal void ClearRestock()
        {
            Main.MainForm.tbRestock.Text = "";
            RestockNpc = null;
        }

        internal void ClearRepair()
        {
            Main.MainForm.tbRepair.Text = "";
            RepairNpc = null;
        }

        internal void ClearFactions()
        {
            listFactions.Clear();
            Main.MainForm.tbFactions.Text = "";
            Main.MainForm.lFactionCount.Text = "Count: ";
        }

        internal void ClearIds()
        {
            listIds.Clear();
            Main.MainForm.tbIds.Text = "";
            Main.MainForm.lbIdCount.Text = "Count: ";
        }

        internal void ClearHotspots()
        {
            listHotspots.Clear();
            Main.MainForm.tbHotspots.Text = "";
            Main.MainForm.lHotspotCount.Text = "Count: ";
        }

        internal void ClearRestockItems()
        {
            Main.MainForm.tbRestockItems.Text = "";
            listRestockItems.Clear();
        }

        internal void ClearVendorWaypoints()
        {

            listVendorHotspots.Clear();
            Main.MainForm.tbVendorHotspots.Text = "";
            Main.MainForm.lVendorHotspotCount.Text = "Count: ";
        }

        internal void ClearGhostWaypoints()
        {

            listGhostHotspots.Clear();
            Main.MainForm.tbGhostHotspots.Text = "";
            Main.MainForm.lGhostHotspotCount.Text = "Count: ";
        }

        internal void AddGWP()
        {
            boolAddGhostWaypoint = true;
        }

        internal void AddWaypoint()
        {
            boolAddWaypoint = true;
        }

        internal void AddVendorWaypoint()
        {
            boolAddVendorWaypoint = true;
        }

        internal void AddVendor()
        {
            boolAddVendor = true;
        }

        internal void AddRepair()
        {
            boolAddRepair = true;
        }

        internal void AddRestock()
        {
            boolAddRestock = true;
        }

        internal void AddFaction()
        {
            boolAddFaction = true;
        }

        internal void AddId()
        {
            boolAddId = true;
        }

        internal void AddRestockItem()
        {
            /*
            var tmpForm = new FormRestockItem();
            if (tmpForm.ShowDialog() == DialogResult.OK)
            {
                if (tmpForm.tbItemName.Text.Trim() != "")
                {
                    var tmpItem = new Structs.RestockItem(tmpForm.tbItemName.Text,
                        (int)tmpForm.nudRestockUpTo.Value);
                    listRestockItems.Add(tmpItem);
                    Main.MainForm.tbRestockItems.Text += tmpItem.Item + Environment.NewLine;
                }
            }*/
        }

        private void AddWaypoint(HotspotType parType)
        {
            var tmpVec = ObjectManager.Instance.Player.Position;
            var pos = Lua.Instance.GetText(Strings.PosInfos, "justPos");

            Enums.PositionType posType;


            posType = Main.MainForm.rbHotspot.Checked ? Enums.PositionType.Hotspot : Enums.PositionType.Waypoint;

            switch (parType)
            {
                case HotspotType.Hotspot:
                    posType = Enums.PositionType.Hotspot;
                    listHotspots.Add(Tuple.Create(tmpVec, pos, posType));
                    Main.MainForm.tbHotspots.Text += tmpVec + Environment.NewLine;
                    Main.MainForm.lHotspotCount.Text = "Count: " + listHotspots.Count;
                    boolAddWaypoint = false;
                    if (listHotspots.Count == 1)
                    {
                        profileStart = Lua.Instance.GetText("", Strings.GT_PosInfos);
                        Main.MainForm.lRecording.Text = "Recording" + Environment.NewLine + pos;
                    }
                    break;

                case HotspotType.VendorHotspot:
                    listVendorHotspots.Add(Tuple.Create(tmpVec, pos, posType));
                    Main.MainForm.tbVendorHotspots.Text += tmpVec + Environment.NewLine;
                    Main.MainForm.lVendorHotspotCount.Text = "Count: " + listVendorHotspots.Count;
                    boolAddVendorWaypoint = false;
                    break;

                case HotspotType.Ghost:
                    listGhostHotspots.Add(Tuple.Create(tmpVec, pos, posType));
                    Main.MainForm.tbGhostHotspots.Text += tmpVec + Environment.NewLine;
                    Main.MainForm.lGhostHotspotCount.Text = "Count: " + listGhostHotspots.Count;
                    boolAddGhostWaypoint = false;
                    break;
            }
        }

        private void Mainthread_AddFaction()
        {
            var tmpUnit =
                ObjectManager.Instance.Npcs
                    .FirstOrDefault(i => i.Guid == ObjectManager.Instance.Player.TargetGuid);

            if (tmpUnit != null)
            {
                if (!listFactions.Contains(tmpUnit.FactionId))
                {
                    if (tmpUnit.Reaction != ZzukBot.Constants.Enums.UnitReaction.Friendly)
                    {
                        listFactions.Add(tmpUnit.FactionId);
                        Main.MainForm.tbFactions.Text += tmpUnit.FactionId + Environment.NewLine;
                        Main.MainForm.lFactionCount.Text = "Count: " + listFactions.Count;
                    }
                }
            }
            boolAddFaction = false;
        }

        private void Mainthread_AddId()
        {
            var tmpUnit =
                ObjectManager.Instance.Npcs
                    .FirstOrDefault(i => i.Guid == ObjectManager.Instance.Player.TargetGuid);

            if (tmpUnit != null)
            {
                if (!listIds.Contains(tmpUnit.Id))
                {
                    if (tmpUnit.Reaction != ZzukBot.Constants.Enums.UnitReaction.Friendly)
                    {
                        listIds.Add(tmpUnit.Id);
                        Main.MainForm.tbIds.Text += tmpUnit.Id + Environment.NewLine;
                        Main.MainForm.lbIdCount.Text = "Count: " + listIds.Count;
                    }
                }
            }
            boolAddId = false;
        }

        private void Mainthread_AddNpc(NpcType parType)
        {
            var tmpUnit =
                ObjectManager.Instance.Npcs
                    .FirstOrDefault(i => i.Guid == ObjectManager.Instance.Player.TargetGuid);
            if (tmpUnit == null)
            {
                boolAddRestock = false;
                boolAddVendor = false;
                boolAddRepair = false;
                return;
            }

            var pos = Lua.Instance.GetText(Strings.PosInfos, "justPos");

            switch (parType)
            {
                case NpcType.Repair:
                    RepairNpc = new Structs.NPC(tmpUnit.Name, tmpUnit.Position,
                        pos);
                    Main.MainForm.tbRepair.Text = tmpUnit.Name;
                    boolAddRepair = false;
                    break;

                case NpcType.Restock:
                    RestockNpc = new Structs.NPC(tmpUnit.Name, tmpUnit.Position, pos);
                    Main.MainForm.tbRestock.Text = tmpUnit.Name;
                    boolAddRestock = false;
                    break;

                case NpcType.Vendor:
                    VendorNpc = new Structs.NPC(tmpUnit.Name, tmpUnit.Position, pos);
                    Main.MainForm.tbVendor.Text = tmpUnit.Name;
                    boolAddVendor = false;
                    break;
            }
        }

        private void CreateProfile()
        {

            if (ObjectManager.Instance.IsIngame)
            {

                if (boolAddWaypoint)
                {
                    AddWaypoint(HotspotType.Hotspot);
                }
                if (boolAddVendorWaypoint)
                {
                    AddWaypoint(HotspotType.VendorHotspot);
                }
                if (boolAddGhostWaypoint)
                {
                    AddWaypoint(HotspotType.Ghost);
                }
                if (boolAddFaction)
                {
                    Mainthread_AddFaction();
                }
                if (boolAddId)
                {
                    Mainthread_AddId();
                }
                if (boolAddRepair)
                {
                    Mainthread_AddNpc(NpcType.Repair);
                }
                if (boolAddVendor)
                {
                    Mainthread_AddNpc(NpcType.Vendor);
                }
                if (boolAddRestock)
                {
                    Mainthread_AddNpc(NpcType.Restock);
                }
            }

        }

        private void WriteToXmlProfile()
        {
            if (!Directory.Exists(Paths.ProfileFolder))
                Directory.CreateDirectory(Paths.ProfileFolder);

            var settings = new XmlWriterSettings
            {
                NewLineOnAttributes = true,
                NewLineChars = Environment.NewLine,
                Indent = true,
                IndentChars = "\t"
            };
            using (
                var writer = XmlWriter.Create(
                    Paths.ProfileFolder + "\\" + DateTime.Now.ToString("MMddHHmmss") + ".xml", settings))
            {
                // Start document
                writer.WriteStartDocument();

                // Hotspot Element Start
                writer.WriteComment(" Start: " + profileStart + " ");
                writer.WriteStartElement("Profile");

                writer.WriteStartElement("Hotspots");
                foreach (var vec in listHotspots)
                {
                    writer.WriteStartElement("Hotspot");
                    writer.WriteComment(" Pos: " + vec.Item2 + " ");
                    writer.WriteElementString("X", vec.Item1.X.ToString());
                    writer.WriteElementString("Y", vec.Item1.Y.ToString());
                    writer.WriteElementString("Z", vec.Item1.Z.ToString());
                    writer.WriteElementString("Type", vec.Item3.ToString());
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                if (listFactions.Count != 0)
                {
                    writer.WriteStartElement("Factions");
                    foreach (var f in listFactions)
                    {
                        writer.WriteElementString("Faction", f.ToString());
                    }
                    writer.WriteEndElement();
                }

                if (listIds.Count != 0)
                {
                    writer.WriteStartElement("Ids");
                    foreach (var f in listIds)
                    {
                        writer.WriteElementString("Id", f.ToString());
                    }
                    writer.WriteEndElement();
                }

                if (VendorNpc != null)
                {
                    writer.WriteStartElement("Vendor");
                    writer.WriteComment(" Pos: " + VendorNpc.MapPosition + " ");
                    writer.WriteStartElement("Position");
                    writer.WriteElementString("X", VendorNpc.Coordinates.X.ToString());
                    writer.WriteElementString("Y", VendorNpc.Coordinates.Y.ToString());
                    writer.WriteElementString("Z", VendorNpc.Coordinates.Z.ToString());
                    writer.WriteEndElement();
                    writer.WriteElementString("Name", VendorNpc.Name);
                    writer.WriteEndElement();
                }

                if (RepairNpc != null)
                {
                    if (listVendorHotspots.Count > 0)
                    {
                        writer.WriteStartElement("VendorHotspots");
                        foreach (var vec in listVendorHotspots)
                        {
                            writer.WriteStartElement("VendorHotspot");
                            writer.WriteComment(" Pos: " + vec.Item2 + " ");
                            writer.WriteElementString("X", vec.Item1.X.ToString());
                            writer.WriteElementString("Y", vec.Item1.Y.ToString());
                            writer.WriteElementString("Z", vec.Item1.Z.ToString());
                            writer.WriteElementString("Type", vec.Item3.ToString());
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement();
                    }

                    writer.WriteStartElement("Repair");
                    writer.WriteComment(" Pos: " + RepairNpc.MapPosition + " ");
                    writer.WriteStartElement("Position");
                    writer.WriteElementString("X", RepairNpc.Coordinates.X.ToString());
                    writer.WriteElementString("Y", RepairNpc.Coordinates.Y.ToString());
                    writer.WriteElementString("Z", RepairNpc.Coordinates.Z.ToString());
                    writer.WriteEndElement();
                    writer.WriteElementString("Name", RepairNpc.Name);
                    writer.WriteEndElement();
                }

                if (listGhostHotspots.Count > 0)
                {
                    writer.WriteStartElement("GhostHotspots");
                    foreach (var vec in listGhostHotspots)
                    {
                        writer.WriteStartElement("GhostHotspot");
                        writer.WriteComment(" Pos: " + vec.Item2 + " ");
                        writer.WriteElementString("X", vec.Item1.X.ToString());
                        writer.WriteElementString("Y", vec.Item1.Y.ToString());
                        writer.WriteElementString("Z", vec.Item1.Z.ToString());
                        writer.WriteElementString("Type", vec.Item3.ToString());
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }

                if (RestockNpc != null)
                {
                    if (listRestockItems.Count > 0)
                    {
                        writer.WriteStartElement("Restock");
                        writer.WriteComment(" Pos: " + RestockNpc.MapPosition + " ");
                        writer.WriteStartElement("Position");
                        writer.WriteElementString("X", RestockNpc.Coordinates.X.ToString());
                        writer.WriteElementString("Y", RestockNpc.Coordinates.Y.ToString());
                        writer.WriteElementString("Z", RestockNpc.Coordinates.Z.ToString());
                        writer.WriteEndElement();
                        writer.WriteElementString("Name", RestockNpc.Name);
                        writer.WriteEndElement();

                        writer.WriteStartElement("RestockItems");
                        foreach (var r in listRestockItems)
                        {
                            writer.WriteStartElement("Item");
                            writer.WriteElementString("Name", r.Item);
                            writer.WriteElementString("RestockUpTo", r.RestockUpTo.ToString());
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement();
                    }
                }
                // Hotspot Element End
                writer.WriteEndElement();
                // End document
                writer.WriteEndDocument();
            }
        }


        private enum HotspotType
        {
            Hotspot = 1,
            VendorHotspot = 2,
            Ghost = 3
        }

        private enum NpcType
        {
            Repair = 1,
            Vendor = 2,
            Restock = 3
        }
    }
}
