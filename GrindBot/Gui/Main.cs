﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrindBot.Constants;
using GrindBot.Engine;
using GrindBot.Engine.ProfileCreation;
using GrindBot.Settings;
using ZzukBot.ExtensionFramework;
using ZzukBot.ExtensionFramework.Classes;

namespace GrindBot.Gui
{
    public partial class Main : Form
    {
        private Action _callback;
        internal static Main MainForm;
        public Main(Action parCallBack, int parVersion)
        {
            InitializeComponent();
            PrepareForLaunch();
            _callback = parCallBack;
            bReload_Click(null, null);
            this.Text = this.Text + " v" + parVersion;
        }

        public sealed override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void PrepareForLaunch()
        {
            MainForm = this;
            GrindSettings.Values.Load();

        }

        internal void updateNotification(string parMessage)
        {
            Invoke(new MethodInvoker(delegate
            {
                var count = dgNotifications.Rows.Count;
                dgNotifications.Rows.Add(DateTime.Now.ToString("HH:mm"), parMessage);
                dgNotifications.Rows[count].Cells[1].ToolTipText = parMessage;
                //Monitor.SendChannelMessage(parMessage);
                //PlayBeep();
            }));
        }

        private void bGrindRun_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.None) return;
            Manager.StartGrinder(cbLoadLastProfile.Checked);
        }

        private void bSaveSettings_Click(object sender, EventArgs e)
        {

            GrindSettings.Values.NotifyOnRare = cbNotifyRare.Checked;
            GrindSettings.Values.StopOnRare = cbStopOnRare.Checked;
            GrindSettings.Values.RestManaAt = (int)nudDrinkAt.Value;
            GrindSettings.Values.RestHealthAt = (int)nudEatAt.Value;
            GrindSettings.Values.MobSearchRange = (float)nudMobSearchRange.Value;
            GrindSettings.Values.MaxDiffToWp = (float)nudRoamFromWp.Value;

            //GrindSettings.Values.CombatDistance = (float)nudCombatRange.Value;
            GrindSettings.Values.MinFreeSlotsBeforeVendor = (int)nudFreeSlots.Value;
            GrindSettings.Values.WaypointModifier = (float)nudWaypointModifier.Value;
            GrindSettings.Values.KeepItemsFromQuality = cbKeepQuality.SelectedIndex;
            // (int)Enum.Parse(typeof(Enums.ItemQuality),
            //     (string)cbKeepQuality.SelectedItem);

            GrindSettings.Values.ForceBreakAfter = (int)nudForceBreakAfter.Value;
            GrindSettings.Values.BreakFor = (int)nudBreakFor.Value;


            GrindSettings.Values.ProtectedItems = tbProtectedItems.Text.Split(
                new[] { Environment.NewLine },
                StringSplitOptions.None);


            GrindSettings.Values.SkinUnits = cbSkinUnits.Checked;
            GrindSettings.Values.NinjaSkin = cbNinjaSkin.Checked;
            GrindSettings.Values.Herb = cbHerb.Checked;
            GrindSettings.Values.Mine = cbMine.Checked;
            GrindSettings.Values.LootUnits = cbLootUnits.Checked;

            GrindSettings.Values.MaxResurrectDistance = (float)nudMaxRezDistance.Value;
            GrindSettings.Values.MaxResourceDistance = (float)nudMaxResourceDistance.Value;

            GrindSettings.Values.ForceReturnToPath = cbForceReturnToPath.Checked;
            GrindSettings.Values.TargetSearchWait = (int)nudTargetSearchWait.Value;


            GrindSettings.Values.Save();

        }

        private void bReload_Click(object sender, EventArgs e)
        {
            GrindSettings.Values.Load();

            cbNotifyRare.Checked = GrindSettings.Values.NotifyOnRare;
            cbStopOnRare.Checked = GrindSettings.Values.StopOnRare;

            nudDrinkAt.Value = GrindSettings.Values.RestManaAt;
            nudEatAt.Value = GrindSettings.Values.RestHealthAt;
            nudMobSearchRange.Value = (decimal)GrindSettings.Values.MobSearchRange;
            nudRoamFromWp.Value = (decimal)GrindSettings.Values.MaxDiffToWp;


            nudFreeSlots.Value = GrindSettings.Values.MinFreeSlotsBeforeVendor;
            nudWaypointModifier.Value = (decimal)GrindSettings.Values.WaypointModifier;

            cbKeepQuality.SelectedIndex = GrindSettings.Values.KeepItemsFromQuality;

            nudForceBreakAfter.Value = GrindSettings.Values.ForceBreakAfter;
            nudBreakFor.Value = GrindSettings.Values.BreakFor;

            var itemsToAdd = GrindSettings.Values.ProtectedItems.Aggregate("", (current, item) => current + item + Environment.NewLine);

            tbProtectedItems.Text = itemsToAdd;


            cbSkinUnits.Checked = GrindSettings.Values.SkinUnits;
            cbNinjaSkin.Checked = GrindSettings.Values.NinjaSkin;
            cbHerb.Checked = GrindSettings.Values.Herb;
            cbMine.Checked = GrindSettings.Values.Mine;
            cbLootUnits.Checked = GrindSettings.Values.LootUnits;

            nudMaxRezDistance.Value = (decimal)GrindSettings.Values.MaxResurrectDistance;
            nudMaxResourceDistance.Value = (decimal)GrindSettings.Values.MaxResourceDistance;

            cbForceReturnToPath.Checked = GrindSettings.Values.ForceReturnToPath;
            nudTargetSearchWait.Value = GrindSettings.Values.TargetSearchWait;

        }

        private void bSettingsHelp_Click(object sender, EventArgs e)
        {
            var str = "Account and Password -> Used for Relog\n";
            str += "Search Mob Range -> Radius around the toon which will searched for the next target\n";
            str += "Combat Range -> Distance at which the bot stops moving towards the target\n";
            str += "Roam from Waypoint -> Distance how far the bot will move away from the current path\n";
            str += "Free Slots -> Start vendoring if free slot count fall under this value\n";
            str +=
                "Waypoint Modifier -> Usually the next waypoint will be loaded if the toon is 1.3 yards or closer to the current one. Very bad computers could cause stuttering. You can modify the value at which a next waypoint will be loaded by a value from -0.5 (0.8) to 0.5 (1.8) to counter this";

            MessageBox.Show(str, "Help");
        }

        private void bCreate_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.None) return;
            Manager.StartProfileCreation();
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.StopCurrentEngine();
        }

        private void bCreateHelp_Click(object sender, EventArgs e)
        {
            var str =
    "Hotspots -> Add hotspots around the area you like to grind. The bot will automatically build paths around those\n";
            str += "Vendor Hotspots -> Visit the forums for a guide on how to use those\n";
            str += "Factions -> Target a mob which the bot should kill and add its faction\n";
            str +=
                "Every position related profile item will be saved with its ingame coordinates inside the profile to ease up the task of editing existing profiles\n";
            str += "If no factions are added the bot will attack any mob which isnt friendly";

            MessageBox.Show(str, "Help");
        }

        private void bAddHotspots_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().AddWaypoint();
        }

        private void bClearHotspots_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().ClearHotspots();
        }

        private void bAddVendorHotspot_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().AddVendorWaypoint();
        }

        private void bClearVendorHotspots_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().ClearVendorWaypoints();
        }

        private void bAddGhostHotspot_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().AddGWP();
        }

        private void bClearGhostHotspots_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().ClearGhostWaypoints();
        }

        private void bAddRestockItem_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().AddRestockItem();
        }

        private void bClearRestockItems_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().ClearRestockItems();
        }

        private void bAddFaction_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().AddFaction();
        }

        private void bClearFactions_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().ClearFactions();
        }

        private void bAddRepair_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().AddRepair();
        }

        private void bClearRepair_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().ClearRepair();
        }

        private void bAddRestock_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().AddRestock();
        }

        private void bClearRestock_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().ClearRestock();
        }

        private void bAddVendor_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().AddVendor();
        }

        private void bClearVendor_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().ClearVendor();
        }


        //#region Modify, Save and load settings
        /// <summary>
        ///     Delegate used to update controls on the form
        /// </summary>
        internal void UpdateControl<T>(object Value, T control) where T : Control
        {
            if (control.GetType() == typeof(TextBox))
            {
                if (Value.GetType() == typeof(string[]))
                {
                    (control as TextBox).Text = "";
                    foreach (var x in (string[])Value)
                    {
                        (control as TextBox).Text
                            += x + Environment.NewLine;
                    }
                }
                else if (Value is string)
                {
                    (control as TextBox).Text = (string)Value;
                }
            }
            else if (control.GetType() == typeof(NumericUpDown))
            {
                var val = Convert.ToDecimal(Value);
                if (val < (control as NumericUpDown).Minimum)
                    // ReSharper disable once RedundantAssignment
                    val = (control as NumericUpDown).Minimum;
                (control as NumericUpDown).Value = Convert.ToDecimal(Value);
            }
            else if (control.GetType() == typeof(ComboBox))
            {
                var i = 0;
                for (; i < (control as ComboBox).Items.Count; i++)
                {
                    if (
                        (string)(control as ComboBox).Items[i] ==
                        ((Enums.ItemQuality)Value).ToString())
                    {
                        break;
                    }
                }
                (control as ComboBox).SelectedIndex = i;
            }
            else if (control.GetType() == typeof(Label))
            {
                (control as Label).Text = (string)Value;
            }
            else if (control.GetType() == typeof(CheckBox))
            {
                (control as CheckBox).Checked = (bool)Value;
            }
        }

        private void bGrindStop_Click(object sender, EventArgs e)
        {
            Manager.StopCurrentEngine();
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            _callback();
        }

        private void bShowCC_Click(object sender, EventArgs e)
        {
            if (CustomClasses.Instance.Current != null)
            {
                CustomClasses.Instance.Current.ShowGui();
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void tpProfile_Click(object sender, EventArgs e)
        {

        }

        private void bAddId_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().AddId();
        }

        private void bClearId_Click(object sender, EventArgs e)
        {
            if (Manager.CurrentEngineType != Engines.ProfileCreation) return;
            Manager.EngineAs<ProfileCreator>().ClearIds();
        }
    }
}
